# Car Connect FE Challenge

This application uses React, Typescript, Redux, Yup, React-Hook-Form and Axios. In this application I used form configuration principle, so that it will be easy to add fields in the future use. The application will send the data to the API in the other repo that I created.