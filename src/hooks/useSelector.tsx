import { useSelector as useSelectorSlice } from "../store/slices";
import { States } from "../types/hooks";

export const useSelector = (name: States) => {
  const state = useSelectorSlice((state) => state[name]);
  return state;
};
