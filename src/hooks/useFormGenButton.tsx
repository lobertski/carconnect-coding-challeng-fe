import { MouseEvent } from "react";
import { useDispatch } from "react-redux";

import { States } from "../types/hooks";
import { setStepForm } from "../store/slices/formSlice";
import { useSelector } from ".";

export const useFormButton = (action: string) => {
  const dispatch = useDispatch();
  const { currentStep } = useSelector(States.Form);
  const handleButton = (e?: MouseEvent) => {
    e?.preventDefault();
    dispatch(setStepForm(currentStep + 1));
  };
  switch (action) {
    case "next":
      handleButton();
      break;
    default:
      return () => {};
  }
};
