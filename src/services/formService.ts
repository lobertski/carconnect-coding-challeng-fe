import axios from "axios";

const formAPI = axios.create({
  baseURL: "http://localhost:3003",
});

export const postFormAPI = async (payload: Record<string, unknown>) => {
  const response = await formAPI.post("/users/", { ...payload });
  return response.data;
};

export const getFormAPI = async (userId: number) => {
  const response = await formAPI.get(`/user/${userId}`);
  return response.data;
};
