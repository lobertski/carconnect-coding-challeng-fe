import { PayloadAction, createSlice } from "@reduxjs/toolkit";
import { IFormValues, IFormState } from "../../types/store";

const initialState: IFormState = {
  formValues: {},
  currentStep: 0,
  formID: "",
  steps: [],
  loading: false,
  error: "",
};

const formSlice = createSlice({
  name: "form",
  initialState,
  reducers: {
    setStepForm: (state, { payload }) => {
      state.currentStep = payload;
    },
    setFirstMount: (
      state,
      {
        payload: { formID, steps },
      }: PayloadAction<{ formID: string; steps: [] }>
    ) => {
      state.formID = formID;
      state.steps = steps;
    },
    submitFormRequest: (state, { payload }: PayloadAction<IFormValues>) => {
      state.error = "";
      state.loading = true;
    },
    submitFormSuccess: (state, { payload }: PayloadAction<IFormValues>) => {
      state.loading = false;
      state.formValues = payload;
    },
    submitFormError: (state, { payload }: PayloadAction<string>) => {
      state.error = payload;
      state.loading = false;
    },
  },
});

export const {
  setStepForm,
  setFirstMount,
  submitFormError,
  submitFormRequest,
  submitFormSuccess,
} = formSlice.actions;
export default formSlice.reducer;
