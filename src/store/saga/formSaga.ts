/* eslint-disable @typescript-eslint/no-explicit-any */
import { call, put, takeLatest, select } from "redux-saga/effects";
import { postFormAPI } from "../../services";
import {
  setStepForm,
  submitFormError,
  submitFormRequest,
  submitFormSuccess,
} from "../slices/formSlice";

function* submitFormSage(
  action: Record<string, any>
): Generator<any, any, any> {
  try {
    const { currentStep } = yield select((state) => state.form);
    const response = yield call(postFormAPI, action.payload);
    yield put(submitFormSuccess(response));
    yield put(setStepForm(currentStep + 1));
  } catch (error: any) {
    yield put(submitFormError(error.message));
    alert(error.message);
  }
}

export function* formSaga() {
  yield takeLatest(submitFormRequest.type, submitFormSage);
}
