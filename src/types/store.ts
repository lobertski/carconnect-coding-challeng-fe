export interface RootState {
  form: IFormState;
}

export interface IFormState {
  formValues: IFormValues | Record<string, number | string>;
  currentStep: number;
  formID: string;
  steps: [];
  loading: boolean;
  error: string;
}

export interface IFormValues {
  id?: number;
  first_name: string;
  last_name: string;
  email: string;
  phone_number: string;
  postcode: string;
}
