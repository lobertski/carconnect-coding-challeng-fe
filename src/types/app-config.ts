import { Control, FieldErrors } from "react-hook-form";
import { AnySchema } from "yup";
export interface IFieldList {
  id: string;
  type: string;
  component: string;
  label: string;
  required: boolean;
  hidden: boolean | { when: string; is: (value: string) => boolean };
  defaultValue: string;
  value: string;
  maxLength?: number;
  placeholder: string;
  style: {
    width: string;
  };
  validation: AnySchema;
}

export interface IButtons {
  label: string;
  action: string;
  style: Record<string, unknown>;
}

export interface ISection {
  sectionName: string;
  sectionID: string;
  fieldList: IFieldList[];
}

export interface IForm {
  formID: string;
  type: string;
  steps: {
    id: string;
    label: string;
    component: string;
  }[];
  formSections: ISection[];
  buttons: IButtons[];
}

export interface InputsProps {
  field: IFieldList;
  control: Control<any>;
  errors: FieldErrors<Record<string, any>>;
  hidden: boolean;
}

export type ComponentType = "ApplicationForm" | "ReviewPage" | "SucessPage";
