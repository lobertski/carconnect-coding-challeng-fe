import * as yup from "yup";
const numberRegex = /^[0-9]+$/;
const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

const mockSectionForm = [
  {
    sectionName: "Personal Information",
    sectionID: "personal-info",
    fieldList: [
      {
        id: "first_name",
        type: "text",
        component: "text-field",
        label: "First Name",
        required: true,
        defaultValue: "",
        hidden: false,
        value: "",
        placeholder: "",
        style: {
          width: "100%",
        },
        validation: yup.string().required("First Name is required."),
      },
      {
        id: "last_name",
        type: "text",
        component: "text-field",
        label: "Last Name",
        required: true,
        defaultValue: "",
        hidden: false,
        value: "",
        placeholder: "",
        style: {
          width: "100%",
        },
        validation: yup.string().required("Last Name is required."),
      },
    ],
  },
  {
    sectionName: "Contact Information",
    sectionID: "contact-info",
    fieldList: [
      {
        id: "email",
        type: "email",
        component: "text-field",
        label: "Email",
        required: true,
        defaultValue: "",
        hidden: false,
        value: "",
        placeholder: "",
        style: {
          width: "100%",
        },
        validation: yup
          .string()
          .matches(emailRegex, "Please enter a valid email")
          .required("Email is required."),
      },
      {
        id: "phone_number",
        type: "text",
        component: "text-field",
        label: "Phone Number",
        required: true,
        defaultValue: "",
        hidden: false,
        value: "",
        placeholder: "",
        style: {
          width: "100%",
        },
        validation: yup
          .string()
          .required("Phone number is required.")
          .matches(numberRegex, "Please enter a valid phone number.")
          .min(10, "Phone number must be 10 digits.")
          .max(10, "Phone number must be 10 digits."),
      },
      {
        id: "postcode",
        type: "text",
        component: "text-field",
        label: "Postcode",
        required: true,
        defaultValue: "",
        hidden: false,
        value: "",
        placeholder: "",
        maxLength: 4,
        style: {
          width: "100%",
        },
        validation: yup
          .string()
          .required("Postcode is required.")
          .matches(numberRegex, "Please enter a valid postcode.")
          .min(4, "Postcode must be 4 digits.")
          .max(4, "Postcode must be 4 digits."),
      },
    ],
  },
];

export const basicMockForm = {
  formID: "basic-form",
  type: "form",
  steps: [
    {
      id: "first-step",
      label: "Application Form",
      component: "ApplicationForm",
    },
    {
      id: "review-step",
      label: "Review",
      component: "ReviewPage",
    },
    {
      id: "final-step",
      label: "Success",
      component: "SucessPage",
    },
  ],
  formSections: [...mockSectionForm],
  buttons: [
    {
      label: "Next",
      action: "next",
      style: {},
    },
  ],
};
