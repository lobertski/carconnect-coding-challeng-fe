import { MDBBtn } from "mdb-react-ui-kit";
export const SucsessPage = () => {
  return (
    <>
      <img
        src="https://cdn-icons-png.flaticon.com/512/10015/10015365.png"
        style={{ width: "3rem", height: "3rem" }}
      ></img>
      <h3 style={{ color: "#1F6CC9", padding: "1rem" }}>Success!</h3>
      <MDBBtn onClick={() => window.location.reload()}>Create Again</MDBBtn>
    </>
  );
};
