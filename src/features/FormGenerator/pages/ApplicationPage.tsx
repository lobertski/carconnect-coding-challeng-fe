/* eslint-disable @typescript-eslint/no-explicit-any */
import { Fragment, useEffect } from "react";
import { useDispatch } from "react-redux";
import { useForm } from "react-hook-form";
import * as yup from "yup";
import { MDBTypography, MDBContainer, MDBRow } from "mdb-react-ui-kit";
import { yupResolver } from "@hookform/resolvers/yup";

import { Button, Divider, Inputs } from "../components";
import {
  setFirstMount,
  setStepForm,
  submitFormRequest,
} from "../../../store/slices/formSlice";
import { useSelectedForm, useSelector } from "../../../hooks";
import { States, IFieldList, ISection } from "../../../types";

const generateFieldValidations = (formSections: ISection[]) => {
  const validations = formSections.reduce(
    (accumulator: Record<string, any>, section: ISection) => {
      section.fieldList.forEach((field: IFieldList) => {
        accumulator[field.id] = field.validation;
      });
      return accumulator;
    },
    {}
  );
  return yup.object().shape({
    ...validations,
  });
};

export const ApplicationPage = () => {
  const { buttons, formSections, steps } = useSelectedForm("basic-form");
  const { currentStep } = useSelector(States.Form);
  const dispatch = useDispatch();
  const schema = generateFieldValidations(formSections);
  const {
    handleSubmit,
    control,
    watch,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });

  useEffect(() => {
    dispatch(setFirstMount({ formID: "basic-form", steps }));
  }, []);

  const handleButtonClick = () => {
    handleSubmit(onSubmit)();
  };

  const onSubmit = (data: any) => {
    dispatch(submitFormRequest(data));
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)} className="form-container">
      <MDBContainer className="form-card">
        {formSections.map(({ sectionName, fieldList }: ISection) => (
          <MDBRow key={sectionName}>
            <MDBTypography className="form-title" tag="h4">
              {sectionName}
            </MDBTypography>
            {fieldList.map((field: IFieldList) => (
              <Fragment key={field.id}>
                <Inputs
                  errors={errors}
                  control={control}
                  field={field}
                  hidden={
                    typeof field.hidden === "object"
                      ? field.hidden.is(watch(field.hidden.when))
                      : field.hidden
                  }
                />
              </Fragment>
            ))}
          </MDBRow>
        ))}
        <Button handleButtonClick={handleButtonClick} buttons={buttons} />
      </MDBContainer>
      <Divider />
    </form>
  );
};
