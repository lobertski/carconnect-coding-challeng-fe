import { useCallback } from "react";
import { useDispatch } from "react-redux";
import { MDBBtn } from "mdb-react-ui-kit";
import Stack from "react-bootstrap/Stack";

import { useSelector } from "../../../hooks";
import { States, IButtons } from "../../../types";
import { setStepForm } from "../../../store/slices/formSlice";

export const Button = ({
  buttons,
  handleButtonClick,
}: {
  buttons: IButtons[];
  handleButtonClick?: () => void;
}) => {
  const dispatch = useDispatch();
  const { currentStep } = useSelector(States.Form);
  const handleStep = useCallback(() => {
    if (handleButtonClick) {
      return handleButtonClick();
    }
    dispatch(setStepForm(currentStep + 1));
  }, []);

  return (
    <Stack>
      {buttons.map(({ label }: IButtons) => (
        <MDBBtn key={label} onClick={handleStep} className="stack-container">
          {label}
        </MDBBtn>
      ))}
    </Stack>
  );
};
